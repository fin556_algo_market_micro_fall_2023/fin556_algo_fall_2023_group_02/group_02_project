# List of Tickers

## Individual Stocks
* AAPL - Apple Inc.
* AMZN - Amazon.com, Inc.
* AXP - American Express Company
* BA - Boeing Company
* CAT - Caterpillar Inc.
* CSCO - Cisco Systems Inc.
* CVX - Chevron Corporation
* DIS - Walt Disney Company
* DOW - Dow Inc.
* FB - Meta Platforms, INC. (Facebook)
* GOOG - Alphabet Inc. (Class C)
* GOOGL - Alphabet Inc. (Class A)
* GS - Goldman Sachs Group, Inc.
* HD - Home Depot, Inc.
* IBM - International Business Machines Corporation
* INTC - Intel Corporation
* JNJ - Johnson & Johnson
* JPM - JP Morgan Chase & CO.
* KO - Coca-Cola Company
* MCD - MCDonald's Corporation
* MMM - 3M Company
* MRK - Merck & Company, Inc.
* MSFT - Microsoft Corporation
* NKE - Nike, Inc.
* PFE - Pfizer, Inc.
* PG - Procter & Gamble Company
* TRV - The Travelers Companies, Inc.
* UNH - UnitedHealth Group Incorporated
* UTX - United Technologies Corporation
* V - Visa Inc.
* VZ - Verizon Communications Inc.
* WBA - Walgreens Boots Alliance Inc
* WMT - Walmart Inc.
* XOM - Exxon Mobil Corporation

## ETF/ETN
* DIA - SPDR Dow Jones Industrial Average ETF
* HYG - iShares iBoxx $ High Yield Corporate Bond ETF
* IVV - iShares Core S&P 500 ETF
* LQD - iShares iBoxx $ Inv Grade Corporate Bond ETF
* OILD - MicroSectors Oil & Gas Exp. & Prod. -3x Inverse Leveraged ETN
* PSQ - ProShares Short QQQ
* QQQ - Invesco QQQ Trust Series 1 (Nasdaq-100 Index)
* SDS - ProShares UltraShort S&P500
* SPXL - Direxion Daily S&P 500 Bull 3X Shares
* SPY - SPDR S&P 500 ETF Trust
* TLT - iShares 20+ Year Treasury Bond ETF
* USO - United States Oil Fund
* VOO - Vanguard 500 Index Fund ETF
* VXX - iPath Series B S&P 500 VIX Short-Term Futures ETN
